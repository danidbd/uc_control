/*H**********************************************************************
*
* FILENAME : config.h
*
* DESCRIPTION :
*       configuration file
*
* PUBLIC FUNCTIONS :
*
* NOTES :
*       Configure the system here.
*
* AUTHOR : Daniel Bravo Darriba
*
* CHANGES :
* VERSION DATE    WHO                   DETAIL
* 1.0.0   20Oct18 Daniel Bravo Darriba  Initial
* 1.0.1   04Nov18 Daniel Bravo Darriba  add power on signalazing defines
* 1.0.2   07Nov18 Daniel Bravo Darriba  increase DEBOUNCE_TIME to 40000
*                                       decrease DLY_OFF to 2000
* 1.0.3   08Nov18 Daniel Bravo Darriba  new define: ALARM_COUNTER 10
*H**********************************************************************/

#ifndef CONFIG_H
#define CONFIG_H

//debounce time. Substraction loop
//increase if you have any debounce issues
#define DEBOUNCE_TIME 40000

//Cycles needed to trigger the alarm
#define ALARM_COUNTER 10

//timer config port
#define TMR_CNF_0 4
#define TMR_CNF_1 5
#define TMR_CNF_2 6
#define TMR_CNF_3 7

//delay levels (milliseconds)
#define DLY_0 5000
#define DLY_1 10000
#define DLY_2 20000
#define DLY_3 40000

//delay default (milliseconds)
#define DLY_DFL 5000

//delay off - End of cycle (milliseconds)
#define DLY_OFF 2000

//Power on blinker ton/toff(ms)
#define POWON_TIME 10

//Power on blinker repetitions
#define POWON_TIMES 20

// ---------- I/O defines ----------
//push button input. Attached to interrupt.
//remap ONLY if you what are you doing
//remark: Arduino Nano ext_int only 2 and 3
#define PUSH_IN 2
#define MICRO_IN 3          //micro input
#define ALARM_OUT 11        //alarm output
#define SIG_OUT 12          //control output
#define LED_BI 13           //LED build-in. Linked to status of SIG_OUT
#define BULB_EXT 14         //Bulb external indication

#endif /* CONFIG_H */
