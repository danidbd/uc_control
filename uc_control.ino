/*H**********************************************************************
*
* FILENAME : uc_control.c
*
* DESCRIPTION :
*       Main file
*
* PUBLIC FUNCTIONS :
*       void setup()
*       void loop()
*
* NOTES :
*       Fill system control.
*
* AUTHOR : Daniel Bravo Darriba
*
* CHANGES :
* VERSION DATE    WHO                   DETAIL
* 1.0.0   01Oct18 Daniel Bravo Darriba  Initial implementation
* 1.1.0   15Oct18 Daniel Bravo Darriba  New features. Button on/off
*                                       by interrupt and LED indicator
* 1.1.1   17Oct18 Daniel Bravo Darriba  Fix: 
*                                       - Increase debounce to 20000
*                                       - Modify interface configuration
*                                       - Link bulb output to state machine
*                                         status
*
* 1.1.2   04Nov18 Daniel Bravo Darriba  Feature: add power on signalazing
* 1.1.3   05Nov18 Daniel Bravo Darriba  Feature: interrupt noise protection
* 1.1.4   07Nov18 Daniel Bravo Darriba  Bugfix: move status toogle to the end 
*                                       of interrupt handling routine
* 1.1.5   08Nov18 Daniel Bravo Darriba  Features: add alarm feauture for
*                                       a global desactivation. Alocate
*                                       status toogle into a subroutine
*H**********************************************************************/
#include "config.h"
volatile bool state = false;
volatile int alarm_counter = 0;

void setup()
{
  /* Initial i/o setup */
  pinMode(TMR_CNF_0, INPUT_PULLUP);
  pinMode(TMR_CNF_1, INPUT_PULLUP);
  pinMode(TMR_CNF_2, INPUT_PULLUP);
  pinMode(TMR_CNF_3, INPUT_PULLUP);
  pinMode(MICRO_IN, INPUT_PULLUP);
  pinMode(PUSH_IN, INPUT_PULLUP);
  pinMode(SIG_OUT, OUTPUT);
  pinMode(LED_BI, OUTPUT);
  pinMode(BULB_EXT, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(PUSH_IN), int_handler, FALLING);
  // Power on signalization
  for(int i=0;i<=POWON_TIMES;i++)
  {
    digitalWrite(LED_BI, HIGH);
    delay(POWON_TIME);
    digitalWrite(LED_BI, LOW);
    delay(POWON_TIME);
  }
}

void loop()
{
  if (!digitalRead(MICRO_IN) && state)
  {
    setOutputs(HIGH);
    wait(DLY_DFL); // avoid no delay scenario    
    if (!digitalRead(TMR_CNF_0))
      wait(DLY_0);
    if (!digitalRead(TMR_CNF_1))
      wait(DLY_1);
    if (!digitalRead(TMR_CNF_2))
      wait(DLY_2);
    if (!digitalRead(TMR_CNF_3))
      wait(DLY_3);
    setOutputs(LOW);
    wait(DLY_OFF);
    alarm_counter++;
    if (alarm_counter==ALARM_COUNTER)
      trigger_alarm();
  }
  else
  {
    alarm_counter=0;
  }
}
/*
 * Function:  int_handler 
 * --------------------
 *  External interrupt handler. Change the state machine status
 *  Toggle status and includes debounce protection
 *
 *  returns: -
 */
void int_handler()
{
  unsigned int debounce=DEBOUNCE_TIME;
  while(debounce) //debounce loop
    debounce--;
  if(digitalRead(PUSH_IN))
    return; //Interrupt produced by some noise
  toogleState();
  while(!digitalRead(PUSH_IN)) //keep while low
    debounce=DEBOUNCE_TIME;
  while(debounce) //debounce loop
    debounce--;
}

/*
 * Function:  wait 
 * --------------------
 *  implementation of a delay but only 1ms busy cycles
 *  
 *   countdown: delay duration (ms)
 *
 *  returns: -
 */
void wait(unsigned int countdown)
{
  while(countdown && state)
  {
    countdown--;
    delay(1);  
  }
}

/*
 * Function:  setOutputs 
 * --------------------
 *  container to set several i/o in 
 *  the same subrutine call
 *  
 *   level: boolean HIGH or LOW
 *
 *  returns: -
 */
void setOutputs(bool level)
{
  digitalWrite(SIG_OUT, level);
  digitalWrite(LED_BI, level);
}

/*
 * Function:  toogleState() 
 * --------------------
 *  container to set several i/o in 
 *  the same subrutine call 
 *  
 *  returns: -
 */
void toogleState()
{
  state=!state;
  digitalWrite(BULB_EXT, state);

  //Reset alarm
  alarm_counter=0;
  digitalWrite(ALARM_OUT, LOW);
}

/*
 * Function:  trigger_alarm() 
 * --------------------
 *  container to set several i/o in 
 *  the same subrutine call 
 *  
 *  returns: -
 */
void trigger_alarm()
{
  state=false;
  setOutputs(LOW);
  digitalWrite(BULB_EXT, state);
  digitalWrite(ALARM_OUT, HIGH);
}
